###############################################################################
# A TEST FUNCTION
# Run with PyTest
###############################################################################

import turtlepack as tp

v = tp.functions.func(5)
m = tp.functions.Model(v)
m.run()

def test_model():
    """
    Tests the behavior of the Model class.
    """
    assert m.value == 731
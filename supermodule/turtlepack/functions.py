#  A MODULE
def func(x):
    """
    A function.
    """
    return x**2 + 2


class Model:
    """
    A dummy model class.
    """
    def __init__(self, value = 0.):
        self.value = value
        
    def run(self):
        """
        Runs the model.
        """
        self.value = func(self.value)
        
    def half(self):
        """
        Divides current value by 2
        """
        self.value=self.value/2;
# A DEMO FILE
#import sys
#sys.path.append("supermodule/") # Temporary modification on PYTHONPATH
#from turtlepack.functions import func
import turtlepack as tp

v = tp.functions.func(5)

m = tp.functions.Model(v)
m2 = tp.functions.Model(v+1)
m.run()

print(m.value)